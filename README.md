# platform service
Full steps to build back the solution are describes on this notion page https://accidental-shape-14b.notion.site/C-Microservices-b8e71bf243464527996f38fdddf9ded3?pvs=4 
Or you can too watch the full tutorials https://www.youtube.com/watch?v=DgVjEo3OGBI 

![Alt text](architecture.png?raw=true "Archi of the system")

# Getting Started
- `cd K8S`
- `kubectl apply -f .\commands-depl.yaml` idem for all other .yaml files inside
- Update your windows host file to link "acme.com" domaine to your local ip by adding `127.0.0.1 acme.com`
- execute this curl `curl --request GET \
  --url http://acme.com/api/c/platforms`
- Reading the logs of the commands-deployment (inside docker desktop for exemple), you can see that the command service fetch using GRPC the missing platforms from the Platfrom Service.
- Executing this curl `curl --request POST \
  --url http://acme.com/api/platforms \
  --header 'Content-Type: application/json' \
  --data '{
	"name": "a new fictive platform",
	"publisher": "David",
	"cost": "1000$"
}'` you can see that adding a platform on the Platform Service API trigger an event throught RabbitMQ and add too the new Platform to the Command Service Database. 

# CLI Cheatsheet
Here you can find just a cheatsheet of commands. 
## docker
- `docker --version`
### platform service
- `cd PlatformService`
- `docker build -t dbconception/platformservice .`
- `docker run -p 8080:80 -d dbconception/platformservice`
- `docker push dbconception/platformservice`
### commands service
- `cd CommandsService`
- `docker build -t dbconception/commandservice .`
- `docker run -p 8080:80 dbconception/commandservice`
- `docker push dbconception/commandservice`

## K8s
### check if enabled
- `kubectl version`
### Lunch Deployment configuration
- `kubectl apply -f .\platforms-depl.yaml`
- `kubectl apply -f .\commands-depl.yaml`
- `kubectl get deployments`
- `kubectl get pods`
### To force to refresh the deployment 
- `kubectl rollout restart deployment platforms-depl`
### Kill auto-deployment
- `kubectl delete deployment platforms-depl`
### Node Port Service - Only for dev env 
#### Lunch Node Port service
(Warning : Deployment shoul be lunched before)
- `kubectl apply -f .\platforms-np-srv.yaml`
#### Read the port of node port service
- `kubectl get services`
#### Delete the service
- `kubectl delete services platformnpservice-srv`
### Deal with Namespace
- `kubectl get namespace`
- `kubectl get pods --namespace=ingress-nginx`
## Setup load balancer 
- `kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.8.0/deploy/static/provider/aws/deploy.yaml`
- `kubectl apply -f .\ingress-srv.yaml`
## Setup sql server
- `kubectl apply -f .\platforms-depl.yaml`
- `kubectl create secret generic mssql --from-literal=SA_PASSWORD="pa55w0rd!"` where SA_PASSWORD is a key to store the password "pa55w0rd!"