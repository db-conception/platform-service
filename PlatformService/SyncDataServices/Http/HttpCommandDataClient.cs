using System.Text;
using System.Text.Json;
using PlatformService.Dtos;

namespace PlatformService.SyncDataServices.Http
{
    public class HttpCommandDataClient : ICommandDataClient
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;

        public HttpCommandDataClient(HttpClient httpClient, IConfiguration configuration)
        {
            _httpClient = httpClient;
            _configuration = configuration;
        }
        public async Task SendPlatformToCommand(PlatformReadDto platformReadDto)
        {
            var httpContent = new StringContent(
                JsonSerializer.Serialize(platformReadDto),
                Encoding.UTF8,
                "application/json"
            );

            string commandServiceBaseUri = _configuration["CommandService"];
            System.Console.WriteLine("We read from appSetting the next base uri : " + commandServiceBaseUri);
            var response = await _httpClient.PostAsync($"{commandServiceBaseUri}/api/c/platforms/", httpContent);

            if(response.IsSuccessStatusCode){
                System.Console.WriteLine("--> Sync POST to CommandService was OK !");
            }
            else {
                System.Console.WriteLine("--> Sync POST to CommandService was NOK !");
            }
        }
    }
}