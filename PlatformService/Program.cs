using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using PlatformService.AsyncDataService;
using PlatformService.Data;
using PlatformService.SyncDataServices.Grpc;
using PlatformService.SyncDataServices.Http;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Add services depending of environement
if (builder.Environment.IsProduction())
{
    System.Console.WriteLine("--> Production connection");
    builder.Services.AddDbContext<AppDbContext>(opt => opt.UseSqlServer(builder.Configuration.GetConnectionString("PlatformsConn")));
}
else if (builder.Environment.IsDevelopment())
{
    System.Console.WriteLine("--> Development connection");
    builder.Services.AddDbContext<AppDbContext>(opt => opt.UseInMemoryDatabase("InMem"));
}
builder.Services.AddScoped<IPlatformRepo, PlatformRepo>();
builder.Services.AddHttpClient<ICommandDataClient, HttpCommandDataClient>();
builder.Services.AddSingleton<IMessageBusClient, MessageBusClient>();
builder.Services.AddGrpc();
builder.Services.AddControllers();
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

Console.WriteLine($"--> endpoint {builder.Configuration["CommandService"] ?? "[null]"}");
var app = builder.Build();



// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// app.UseHttpsRedirection(); not necessary inside the cluster

app.UseAuthorization();

PrepDb.PrepPopulation(app, app.Environment.IsProduction());

app.MapControllers();
app.MapGrpcService<GrpcPlatformService>();
// optional : it's a good pratice to provide the contract
app.MapGet("/protos/platforms.proto", async context => {
    await context.Response.WriteAsync(System.IO.File.ReadAllText("Protos/platforms.proto"));
});
app.Run();
