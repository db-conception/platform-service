using System.Text;
using System.Text.Json;
using AutoMapper;
using PlatformService.Dtos;
using RabbitMQ.Client;

namespace PlatformService.AsyncDataService{

    public class MessageBusClient : IMessageBusClient
    {
        private readonly IConfiguration _configuration;
        private readonly IConnection _connection;
        private readonly IModel _channel;

        public MessageBusClient(IConfiguration configuration, IMapper mapper)
        {
            _configuration = configuration;

            int rabbitPort;
            ConnectionFactory factory;
            if(int.TryParse(_configuration["RabbitMQPort"], out rabbitPort)){
                factory = new ConnectionFactory() {
                    HostName = _configuration["RabbitMQHost"],
                    Port = rabbitPort
                };
            }
            else {
                System.Console.WriteLine("--> Rabbit MQ Port in not an integer");
                throw new Exception("--> Rabbit MQ Port in not an integer");
            }

            try
            {
                _connection = factory.CreateConnection();
                _channel = _connection.CreateModel();

                _channel.ExchangeDeclare(exchange: "trigger", type: ExchangeType.Fanout);
                _connection.ConnectionShutdown += RabbitMQ_ConnectionShutdown;
            }
            catch (System.Exception e)
            {
                System.Console.WriteLine($"--> Could not connect to the bus {e.Message}");
            }
        }

        public void PublishNewPlatform(PlatformPublishDto platform)
        {
            var message = JsonSerializer.Serialize(platform);

            if(_connection.IsOpen){
                System.Console.WriteLine("--> RabbitMQ connection Open, sending message...");
                SendMessage(message);
            }
            else {
                System.Console.WriteLine("--> RabbitMQ connection is closed");
            }
        }

        private void SendMessage(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);

            _channel.BasicPublish(exchange: "trigger", routingKey:"", basicProperties: null, body: body);
            System.Console.WriteLine($"--> message sent.\n{message}");
        }

        public void Dispose()
        {
            if(_channel.IsClosed)
            {
                _channel.Close();
                _connection.Close();
            }
            System.Console.WriteLine("MessageBus Disposed");
        }

        private void RabbitMQ_ConnectionShutdown(object sender, ShutdownEventArgs ev){
            System.Console.WriteLine("--> Our connection has been shot");
        }
    }
}