using System.Text;
using CommandsService.EventProcessing;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace CommandsService.AsyncDataServices
{
    public class MessageBusSubscriber : BackgroundService
    {
        private readonly IConfiguration configuration;
        private readonly IEventProcessor eventProcessor;
        private IConnection connection;
        private IModel channel;
        private string queueName;

        public MessageBusSubscriber(
            IConfiguration configuration,
            IEventProcessor eventProcessor)
        {
            this.configuration = configuration;
            this.eventProcessor = eventProcessor;

            this.InitializeRabbitMQ();
        }

        private void InitializeRabbitMQ()
        {
            var factory = new ConnectionFactory()
            {
                HostName = this.configuration["RabbitMQHost"],
                Port = int.TryParse(configuration["RabbitMQPort"], out int parsedPort) ? parsedPort : -1
            };

            if(factory.Port == -1){
                throw new Exception("Can not parse the port configured. It must be an integer but we get " + configuration["RabbitMQPort"]);
            }

            this.connection = factory.CreateConnection();
            this.channel = this.connection.CreateModel();
            this.channel.ExchangeDeclare("trigger", type: ExchangeType.Fanout);
            queueName = this.channel.QueueDeclare().QueueName;
            this.channel.QueueBind(queue: queueName, exchange: "trigger", routingKey: "");
            
            System.Console.WriteLine("--> Listening on the Message Bus ...");

            this.connection.ConnectionShutdown += RabbitMQ_ConnectionShutdown;
        }

        private void RabbitMQ_ConnectionShutdown(object? sender, ShutdownEventArgs e)
        {
            System.Console.WriteLine($"--> Connection shutdown");
        }

        public override void Dispose()
        {
            if(this.channel.IsOpen)
            {
                this.channel.Close();
                this.connection.Close();
            }   

            base.Dispose();
        }
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(this.channel);

            consumer.Received += (ModuleHandle, ea) => 
            {
                System.Console.WriteLine("--> Event Received!");

                var body = ea.Body;
                var notificationMessage = Encoding.UTF8.GetString(body.ToArray());

                this.eventProcessor.ProcessEvent(notificationMessage);
            };

            this.channel.BasicConsume(queue: this.queueName, autoAck: true, consumer: consumer);

            return Task.CompletedTask;
        }
    }
}