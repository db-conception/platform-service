using CommandsService.Models;
using CommandsService.SyncDataServices.Grpc;

namespace CommandsService.Data{
    public static class PrepDb
    {
        public static void PrepPopulation(IApplicationBuilder applicationBuilder)
        {
            using(var scope = applicationBuilder.ApplicationServices.CreateScope())
            {
                var grpcClient = scope.ServiceProvider.GetService<IPlatformDataClient>();

                var platforms = grpcClient.ReturnAllPlatforms();

                SeedData(scope.ServiceProvider.GetService<ICommandRepo>(), platforms);
            }
        }

        private static void SeedData(ICommandRepo repo, IEnumerable<Platform> platforms)
        {
            System.Console.WriteLine("--> Seeding new platforms");

            foreach (var platform in platforms)
            {
                if(!repo.PlatformExist(platform.ExternalID))
                {
                    repo.CreatePlatfrom(platform);
                }

                repo.SaveChanges();
            }
        }
    }
}